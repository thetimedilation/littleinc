FROM openjdk:12-oracle
EXPOSE 8089
ADD build/docker/app.jar app.jar
CMD java .jar /app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-server","-jar","/app.jar"]
