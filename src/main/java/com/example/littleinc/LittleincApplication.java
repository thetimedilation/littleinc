package com.example.littleinc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LittleincApplication {

    public static void main(String[] args) {
        SpringApplication.run(LittleincApplication.class, args);
    }

}
