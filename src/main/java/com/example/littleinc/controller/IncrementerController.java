package com.example.littleinc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class IncrementerController {

    private int counter;

    @GetMapping(value = "/reset")
    public String resetCounter(@RequestParam(name = "name", required = false, defaultValue = "User") String name, Model model) {
        counter = 0;
        model.addAttribute("name", name);
        model.addAttribute("counter", counter);
        return "reset";
    }

    @GetMapping(value = "/requests")
    public String requestCounter(@RequestParam(name = "name", required = false, defaultValue = "User") String name, Model model) {
        counter += 1;

        model.addAttribute("name", name);
        model.addAttribute("counter", counter);
        return "requests";
    }

}
